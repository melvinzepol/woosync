
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$serverName = '(local)';

$databaseName = 'VENTAS';
$connectionInfo = array('Database'=>$databaseName);

$conn = sqlsrv_connect($serverName,$connectionInfo);

if($conn){echo '<div class="alert alert-success" role="alert">CONECTADO!</div>'; }else{echo 'Connection failure<br />';die(print_r(sqlsrv_errors(),TRUE));}

$sql = "Select * From arc_woo_sync";

$resultado = sqlsrv_query($conn, $sql);

?>

<!DOCTYPE html>
<html>
<head>
    <title>CATEGORIAS</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <h1 align="center" style="font-family: Consolas;">LISTADO DE CATEGORIAS</h1><br>
    <table width="70%" border="1px" align="center" class="table table-striped table-dark">

    <tr align="center">
        <td align="center"><b>ID</b></td>
        <td align="center"><b>ISC CLASS DOS</b></td>
        <td align="center"><b>WEB CATEGORIAS</b></td>
        <td align="center"><b>CONFIGURACIÓN</b></td>
    </tr>
    <?php
        while($datos = sqlsrv_fetch_array($resultado, SQLSRV_FETCH_ASSOC)){
        ?>
            <tr align="center">
                <td ><?php echo $datos["id"]?></td>
                <td><?php echo $datos["isc_cod_cla_dos"]?></td>
                <td><?php echo $datos["web_category"]?></td>
                <td><button class="btn btn-outline-success" id="btn-abrir-popup" class="btn-abrir-popup" > Configuración</button></td>
            </tr>
            <?php
        }

     ?>
    </table>

  <div class="overlay" id="overlay">
			<div class="popup" id="popup">
				<a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
				<h3>SUSCRIBETE</h3>
				<h4>y recibe un cupon de descuento.</h4>
				<form action="">
					<div class="contenedor-inputs">
						<input type="text" placeholder="Nombre">
						<input type="email" placeholder="Correo">
					</div>
					<input type="submit" class="btn-submit" value="Suscribirse">
				</form>
			</div>
		</div>

	<script src="popup.js"></script>

</body>
</html>





