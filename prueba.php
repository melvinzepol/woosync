<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$serverName = '(local)';

$databaseName = 'VENTAS';
$connectionInfo = array('Database'=>$databaseName);

$conn = sqlsrv_connect($serverName,$connectionInfo);

if($conn){echo '<div class="alert alert-success" role="alert">CONECTADO!</div>'; }else{echo 'Connection failure<br />';die(print_r(sqlsrv_errors(),TRUE));}

$sql = "Select * From arc_woo_sync";

$resultado = sqlsrv_query($conn, $sql);

//Ciclo para seleccionar valores sin comas


while ($lal = sqlsrv_fetch_array($resultado, SQLSRV_FETCH_ASSOC)) {
    $arrayc = explode(',', $lal['isc_cod_cla_dos']);

//Select Query
    $tsq = "Select Clasedos.DESCLADOS From Clasedos  Where Clasedos.CODCLADOS  In ('.$arrayc.')";

    $lol = sqlsrv_query($conn, $tsq);

//se desplegaran los resultados en la tabla
    echo "<table border=1>";
    echo "<tr>";
    echo "<th>DESCLADOS</th>";
    echo "</tr>";

    while ($dates = sqlsrv_fetch_array($lol)) {
        echo '<tr>';
        echo '<td>' . $dates['DESCLADOS'] . '</td>';
        echo '</tr>';
    }
    echo "</table>";
}
//Inicia el html

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>CATEGORIAS</title>

        <link rel="shortcut icon" href="https://static.miweb.padigital.es/var/m_9/91/919/28425/437768-icono-de-caja.png" />

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Css y Javascript con las ventanas emergentes-->
        <link rel="stylesheet" href="estilos.css">

        <!-- Css de Boostrap-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <!-- Script de Boostrap-->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

        <!-- Iconos fotawesome -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600|Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">


</head>

<body>

    <h1 align="center" style="font-family: Consolas,sans-serif;">LISTADO DE CATEGORIAS</h1><br>

    <!-- Tabla de categorias -->
    <table width="70%" border="1px" align="center" class="table table-striped table-dark">

        <!-- Encabezados -->
        <tr align="center">

            <!--<td align="center"><b>CODCLADOS</b></td>-->
            <td align="center"><b>DESCLADOS</b></td>
        </tr>

        <?php
            while($dates = sqlsrv_fetch_array($lol, SQLSRV_FETCH_ASSOC)){
        ?>

        <!-- Relleno de tabla con datos de arc_woo_sync y boton configuracion eliminar, agregar -->
        <tr align="center">
            <td><?php echo $dates["DESCLADOS"]?></td>
        </tr>

            <?php  } ?>

    </table>


    <div class="overlay" id="overlay" >
        <div class="popup" id="popup">
            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
            <h3>ELIMINAR CATEGORIAS</h3>
            <form action="">
                <div class="contenedor-inputs">

                    <table width="70%" border="1px" align="center" class="table table-striped table-dark">

                        <tr align="center">
                            <td align="center"><b>ID</b></td>
                            <td align="center"><b>DESCRIPCIÓN</b></td>
                            <td align="center"><b>OPCIONES</b></td>
                        </tr>

                        <tr align="center">
                            <td >1001</td>
                            <td >Consola</td>
                            <td><button class="btn btn-outline-danger">Eliminar</button></td>
                        </tr>

                    </table>
                </div>
            </form>
        </div>
    </div>


    <div class="overlay2" id="overlay2" >
        <div class="popup2" id="popup2">
            <a href="#" id="btn-cerrar-popup2" class="btn-cerrar-popup2"><i class="fas fa-times"></i></a>
            <h3>AGREGAR CATEGORIAS</h3>
            <form action="">
                <div class="contenedor-inputs2">

                    <table width="70%" border="1px" align="center" class="table table-striped table-dark">

                        <tr align="center">
                            <td align="center"><b>ID</b></td>
                            <td align="center"><b>DESCRIPCIÓN</b></td>
                            <td align="center"><b>OPCIONES</b></td>
                        </tr>

                        <tr align="center">
                            <td >4002</td>
                            <td >Adaptador</td>
                            <td><button class="btn btn-outline-info">Agregar</button></td>
                        </tr>

                    </table>
                </div>
            </form>
        </div>
    </div>

    <!--librerias para el drowdrop-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="popup.js"></script>

</body>
</html>
