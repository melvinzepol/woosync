<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
*/

$serverName = "(local)";
$connectionOptions = array("Database"=>"VENTAS");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);

//Query relacion categoria
$qry1 = "SELECT * FROM arc_woo_sync";
$getCats = sqlsrv_query($conn, $qry1);
if ($getCats == FALSE)
 die(FormatErrors(sqlsrv_errors()));

$post_data = [];

while ($catItem = sqlsrv_fetch_array($getCats, SQLSRV_FETCH_ASSOC)) {
	$catstr_arr = explode(',', $catItem['isc_cod_cla_dos']);
	$catstr = '';
	foreach ($catstr_arr AS $catstr_item) {
		$catstr .= "'".$catstr_item."',";
	}
	$catstr = substr($catstr,0,-1);

	print_r($catstr); echo "<br>";
	//Select Query
	$tsql= "SELECT a.CODPROD, DESPROD, CODMEDIDAINV, z.SALDOFINAL, y.DESCLAUNO AS PROVEEDOR, x.PRECIO
	FROM Productos a
	LEFT JOIN INVSALDOPROD z
	ON z.CODPROD = a.CODPROD
	LEFT JOIN Claseuno y
	ON y.CODCLAUNO = a.CODCLAUNO
	LEFT JOIN Precios x
	ON x.CODPROD = a.CODPROD
	WHERE a.CODCLADOS IN (".$catstr.")
	AND z.INV_ANO = YEAR(GETDATE()) AND z.INV_MES = MONTH(GETDATE());";

	print_r($tsql); echo "<br>";
	//Executes the query
	$getResults= sqlsrv_query($conn, $tsql);
	//Error handling
	if ($getResults == FALSE)
	 die(FormatErrors(sqlsrv_errors()));

	while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		$post_data[] = [
			'sku' => $row['CODPROD'],
			'name'  => utf8_encode($row['DESPROD']),
			'description'  => utf8_encode($row['DESPROD']),
			'short_description' => utf8_encode($row['DESPROD']),
			'category_name' => $catItem['web_category'],
			'quantity' => $row['SALDOFINAL'],
			'price' => $row['PRECIO'],
			'image'  => 'default.jpg',
			'supplier' => $row['PROVEEDOR']
		];
	}
	
}

	$post_data = json_encode($post_data);
	//echo $post_data; 
																															 
	$ch = curl_init('http://tekcity.archangelsystems.com/woosync/');                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                         
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',
		'Content-Length: ' . strlen($post_data))
	);                                                                                                                   
																														 
	$result = curl_exec($ch);
	print_r( $result );

    function FormatErrors($errors)
    {
        /* Display errors. */
        echo "Error information: <br/>";
     
        foreach ($errors as $error) {
            echo "SQLSTATE: ".$error['SQLSTATE']."<br/>";
            echo "Code: ".$error['code']."<br/>";
            echo "Message: ".$error['message']."<br/>";
        }
    }
?>

