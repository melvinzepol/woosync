<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$serverName = '(local)';
$databaseName = 'VENTAS';
$connectionInfo = array('Database'=>$databaseName);
$conn = sqlsrv_connect($serverName,$connectionInfo);
if($conn){echo '<div class="alert alert-success" role="alert">CONECTADO!</div>'; }else{echo 'Connection Fallida<br/>';die(print_r(sqlsrv_errors(),TRUE));}
$sql = "Select * From arc_woo_sync";
$resultado = sqlsrv_query($conn, $sql);



/*
while ($catItem = sqlsrv_fetch_array($resultado, SQLSRV_FETCH_ASSOC)) {
$catstr_arr = explode(',', $catItem['isc_cod_cla_dos']);
$catstr = '';
foreach ($catstr_arr AS $catstr_item) {
    $catstr .= "'".$catstr_item."',";
}
$catstr = substr($catstr,0,-1);
print_r($catstr);echo "<br>";
$tsql = "Select isc_cod_cla_dos from arc_woo_sync where isc_cod_cla_dos in (".$catstr.") ";
print_r($tsql);
$getResults= sqlsrv_query($conn, $tsql);
while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
}}
*/
?>

<!DOCTYPE html>
<html lang="es">
                <head>
                    <title>CATEGORIAS</title>

                    <link rel="shortcut icon" href="https://cdn.icon-icons.com/icons2/1091/PNG/128/safebox_78389.png" />

                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">

                    <!-- Css y Javascript con las ventanas emergentes-->
                    <link rel="stylesheet" href="estilos.css">

                    <!-- Css de Boostrap-->
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

                    <!-- Script de Boostrap-->
                    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

                    <!-- Iconos fotawesome -->
                    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600|Open+Sans" rel="stylesheet">
                    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">


                </head>

                <body>

                <h1 align="center" style="font-family: Consolas,sans-serif;">LISTADO DE CATEGORIAS</h1><br>

                <!-- Tabla de categorias -->
                <table width="70%" border="1px" align="center" class="table table-striped table-dark">

                    <!-- Encabezados -->
                    <tr align="center">
                        <td align="center"><b>CATEGORIA WEB</b></td>
                        <td align="center"><b>CÓDIGOS DE CATEGORIAS ASOCIADAS</b></td>
                        <td align="center"><b>CONFIGURACIÓN</b></td>
                    </tr>

        <?php
            while($dates = sqlsrv_fetch_array($resultado, SQLSRV_FETCH_ASSOC)){

         ?>

        <!-- Relleno de tabla con datos de arc_woo_sync y boton configuracion eliminar, agregar -->
        <tr align="center">
            <td><?php echo $dates["web_category"]?></td>
            <td><?php echo $dates["isc_cod_cla_dos"]?></td>
            <td>
                <?php/*
                $sql2 = "Select * From arc_woo_sync WHERE NOT IN (".$dates['isc_cod_cla_dos'].") WHERE id = ".$dates['id'].";";
                $resul2 = sqlsrv_query($conn, $sql2);
                while($notin = sqlsrv_fetch_array($resul2, SQLSRV_FETCH_ASSOC)){
                    print_r($notin);
                };
                */
                ?>
               <div class='btn-group'>
                    <button type='button' class='btn btn-success dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        Opciones
                    </button>
                    <div class='dropdown-menu'>

                        <button style='cursor: pointer;' class='dropdown-item btn-abrir-popup' data-id="<?php echo $dates["id"]?>" data-codclados="<?php echo $dates["isc_cod_cla_dos"]?>">Eliminar</button>
                        <button style='cursor: pointer;' class='dropdown-item btn-abrir-popup2' data-id="<?php echo $dates["id"]?>" data-codclados="<?php echo $dates["isc_cod_cla_dos"]?>">Agregar</button>
                    </div>
                </div>
            </td>
        </tr>

            <?php } //} ?>

    </table>


    <div class="overlay" id="overlay" >
        <div class="popup" id="popup" style=" overflow: scroll;">
            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
            <h3>ELIMINAR CATEGORIAS</h3>
            <form action="">
                <div class="contenedor-inputs">

                    <table width="70%" border="1px" align="center" class="table table-striped table-dark">

                        <tr align="center">
                            <td align="center"><b>ID</b></td>
                            <td align="center"><b>DESCRIPCIÓN</b></td>
                            <td align="center"><b>OPCIONES</b></td>
                        </tr>

                        <?php

                        while ($catItem = sqlsrv_fetch_array($resultado, SQLSRV_FETCH_ASSOC)) {
                            $catstr_arr = explode(',', $catItem['isc_cod_cla_dos']);
                            $catstr = '';
                            foreach ($catstr_arr AS $catstr_item) {
                                $catstr .= "'".$catstr_item."',";
                            }
                            $catstr = substr($catstr,0,-1);

                            $tsql = "Select DESCLADOS,CODCLADOS From Clasedos where CODCLADOS in (".$catstr.")";

                            $getResults= sqlsrv_query($conn, $tsql);
                            while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {

                            ?>

                        <tr align="center">
                            <td><?php echo $row["CODCLADOS"]?></td>
                            <td><?php echo $row["DESCLADOS"]?></td>
                            <td><button class="btn btn-outline-danger">Eliminar</button></td>
                        </tr>
                        <?php

                        }}

                        ?>
                    </table>
                </div>
            </form>
        </div>
    </div>


    <div class="overlay2" id="overlay2" >
        <div class="popup2" id="popup2" style=" overflow: scroll;">
            <a href="#" id="btn-cerrar-popup2" class="btn-cerrar-popup2"><i class="fas fa-times"></i></a>
            <h3>AGREGAR CATEGORIAS</h3>
            <form action="">
                <input type="hidden" name="id" id="id" />
                <input type="hidden" name="isc_cod_cla_dos" id="isc_cod_cla_dos" />

                <div class="contenedor-inputs2" >

                    <table width="70%" border="1px" align="center" class="table table-striped table-dark">

                        <tr align="center">
                            <td align="center"><b>ID</b></td>
                            <td align="center"><b>DESCRIPCIÓN</b></td>
                            <td align="center"><b>OPCIONES</b></td>
                        </tr>


                        <?php

                        while ($catItem = sqlsrv_fetch_array($resultado, SQLSRV_FETCH_ASSOC)) {
                        $catstr_arr = explode(',', $catItem['isc_cod_cla_dos']);
                            foreach ($catstr_arr AS $catstr_item) {
                                $catstr .= "'".$catstr_item."',";
                            }
                        $catstr = substr($catstr,0,-1);

                        $sql2 = "Select arc_woo_sync.isc_cod_cla_dos from Clasedos,arc_woo_sync where Clasedos.CODCLADOS in ('.$catstr.')";
                        $resul2 = sqlsrv_query($conn, $sql2);

                        while($notin = sqlsrv_fetch_array($resul2, SQLSRV_FETCH_ASSOC)){
                       ?>

                        <tr align="center" >
                            <td ><?php echo $notin["CODCLADOS"]?></td>
                            <td ><?php echo $notin["isc_cod_cla_dos"]?></td>
                            <td><button class="btn btn-outline-info">Agregar</button></td>
                        </tr>

                    <?php

                        } }

                        ?>
                    </table>
                </div>
            </form>
        </div>
    </div>

    <!--librerias para el drowdrop-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="jquery-3.4.1.min.js"></script>
    <script src="popup.js"></script>


</body>
</html>














